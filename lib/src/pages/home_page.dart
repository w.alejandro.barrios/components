import 'package:flutter/material.dart';

import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icon_string_util.dart';

import 'package:componentes/src/pages/alert_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(appBar: AppBar(title: Text('Componentes')), body: _list());
  }

  Widget _list() {
    // print(menuProvider.opciones);

    /* menuProvider.loadData().then((opciones) {
      print(opciones);
    }); */

    return FutureBuilder(
      future: menuProvider.loadData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];
    opciones.add(SizedBox(height: 5.0));

    if (data == null) return [];

    data.forEach((opcion) {
      final widgetTemp = ListTile(
        title: Text(opcion['texto']),
        leading: getIcon(opcion['icon']),
        trailing: Icon(Icons.keyboard_arrow_right, color: Colors.blue),
        onTap: () {
          // Navegacion básica
          /* final route = MaterialPageRoute(builder: (context) => AlertPage()); // AlertPage('', '') -> Podemos enviar argumentos con nombre, posicionales, etc
          Navigator.push(context, route); */

          // Navegación con nombre
          Navigator.pushNamed(context, opcion['ruta']);
        },
      );

      opciones..add(widgetTemp)..add(Divider());
    });

    return opciones;
  }
}
