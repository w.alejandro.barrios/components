import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {

  final _opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes Temp')
      ),
      body: ListView(
        //children: _createItems()
        children: _createItemsShort(),
      )
    );
  }

  List<Widget> _createItems() {
    // Convertir a Widgets
    List<Widget> lista = [];
    for (String opcion in _opciones) {
      final tempWidget = ListTile(
        title: Text(opcion)
      );
      lista..add(tempWidget) // Operador de cascada, para hacer más de un único add.
          ..add(Divider());
    }
    return lista;
  }

  List<Widget> _createItemsShort() {
    return _opciones.map((item) {

      return Column( // Empezar solo con el listTile
        children: <Widget>[
          ListTile(
            title: Text(item),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(Icons.add_alarm),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {},
          ),
          Divider()
        ],
      );

    }).toList();
  }

}