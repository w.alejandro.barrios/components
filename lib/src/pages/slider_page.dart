import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {

  double _sliderPosition = 0;
  bool _blockCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders')
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _createCheckBox(),
            _createSwitch(),
            Expanded(child: _crearImagen())
          ],
        ),
      ),
    );
  }

  Widget _crearSlider() {

    return Slider(
      activeColor: Colors.indigo,
      label: 'Tamaño de la Imagen: $_sliderPosition',
      //divisions: 20,
      value: _sliderPosition,
      min: 0.0,
      max: 400.0,
      onChanged: (_blockCheck) ? null : (value) {
          setState(() {
            _sliderPosition = value;
            print(value);
          });
      });

  }

  Widget _createCheckBox() {

    // Checkbox
    /* return Checkbox(
      value: _blockCheck,
      onChanged: (value) {
        setState(() {
          _blockCheck = value;
        });
      }
      ); */

      return CheckboxListTile(
        title: Text('Bloquear Slider'),
      value: _blockCheck,
      onChanged: (value) {
        setState(() {
          _blockCheck = value;
        });
      }
      );

  }

  Widget _createSwitch() {

      return SwitchListTile(
        title: Text('Bloquear Slider'),
        value: _blockCheck,
        onChanged: (value) {
          setState(() {
          _blockCheck = value;
          });
      }
      );

  }

  Widget _crearImagen() {
    return Image(
      image: NetworkImage('https://toppng.com/uploads/preview/the-amazing-spiderman-vector-logo-free-11574024417b74mq4o9oh.png'),
      width: _sliderPosition,
      fit: BoxFit.contain,
    );
  }
}