import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://lh3.googleusercontent.com/proxy/AgYH_7r12Fy7Vhjv5fiY8t50x2DMYZE908xu41tFzku6gTLLHMXI7mJmenlmriJqx5tv1ZsUk4T4lf_T_Ci2oUovUw11rGRzlTQIWpJL4w'),
              radius: 23.0,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('AB'),
              backgroundColor: Colors.green,
            ),
          )
        ],
      ),
      body: Center (
        child: FadeInImage(
          placeholder: AssetImage('assets/original.gif'),
          image: NetworkImage('https://www.yourtrainingedge.com/wp-content/uploads/2019/05/background-calm-clouds-747964.jpg'),
          fadeInDuration: Duration(milliseconds: 200),
          )
      )
    );
  
  }

}